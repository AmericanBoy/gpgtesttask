﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;

public class GameController : MonoBehaviour
{

#region Singleton class: GameController
    public static GameController Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
#endregion

#region Data

    [SerializeField] private GameObject _car;
    [SerializeField] private List<Transform> _pointsToMoveForCar;
    [SerializeField] private float _speedToMoveForCar;
    [HideInInspector] public bool isMoveCar = true;
    private int currentPointToMove;
    
    [SerializeField] private UIController _canvas;
    [SerializeField] private int _playerCoins = 0;
    [SerializeField] private int _factoryLvl = 1;
    [SerializeField] private List<GameObject> _factoryBuildingOptions;
    [SerializeField] private int _indexFactoryBuild;
    [SerializeField] private int _maxCountUpgradeBuild = 15;
    public Animator _bulldozerAnim;

    private const int FACTORY_UPGRADE_COST = 200;

    private GameSessionData _gameSessionData;

    private ICarMovementService _carMovementService;
    private IApplicationService _applicationService;

#endregion

    [Inject]
    public void Init(ICarMovementService carMovementService, IApplicationService applicationService)
    {
        _carMovementService = carMovementService;
        _applicationService = applicationService;
    }

    private void Start()
    {
        _gameSessionData = _applicationService.GetGameSessionDataFromStorage();
        GetDataGame();

        Observable.FromEvent(h => _applicationService.OnPlayerCoinsChanged += h,
                    h =>  _applicationService.OnPlayerCoinsChanged-= h)
        .Subscribe(_ => 
        {
            _playerCoins += _factoryLvl;
            _canvas.SetCoinstCount(_playerCoins);
        })
        .AddTo(this);

        Observable.FromEvent(h => _applicationService.OnPlayerUpgradeFactory += h,
                    h =>  _applicationService.OnPlayerUpgradeFactory-= h)
        .Subscribe(_ => 
        {
            if((FACTORY_UPGRADE_COST * _factoryLvl) > _playerCoins)
            {
                _canvas.SetActivePanelNotEnoughtCoins();
            }
            else
            {
                if(_factoryLvl != _maxCountUpgradeBuild)
                {
                    _playerCoins = _playerCoins - (FACTORY_UPGRADE_COST * _factoryLvl);
                    _factoryLvl++;
                    _canvas.SetCoinstCount(_playerCoins);
                    _canvas.SetUpgradeCost(FACTORY_UPGRADE_COST * _factoryLvl);
                    _canvas.SetCoinsPerDepivery(_factoryLvl);
                    StartCoroutine(SetFactoryOption());
                    UpgradeFactory();
                }
            }   
        })
        .AddTo(this);
    }

    private void FixedUpdate() 
    {
        if(isMoveCar)
        {
            currentPointToMove = _carMovementService.StartToMoveCar(_car, _pointsToMoveForCar, _speedToMoveForCar, currentPointToMove);          
        }
        else
        {
            _bulldozerAnim.SetBool("playanim", true);
            StartCoroutine(StopCar());
        }
    }

    private void GetDataGame()
    {
        _playerCoins = _gameSessionData.currentCountCoins;
        _factoryLvl = _gameSessionData.currentFactoryLvl;
        _indexFactoryBuild = _gameSessionData.currentIndexFactory;
        CalcTheCoinsEarnedOffline();

        _canvas.SetCoinstCount(_playerCoins);
        _canvas.SetUpgradeCost(FACTORY_UPGRADE_COST * _factoryLvl);
        _canvas.SetCoinsPerDepivery(_factoryLvl);
        UpgradeFactory();
    }

    private void SetDataGame()
    {
        GameSessionData newSessionData = new GameSessionData(_playerCoins, _factoryLvl, DateTime.UtcNow, _indexFactoryBuild);
        _applicationService.UpdateGameSessionData(newSessionData);
    }

    private void UpgradeFactory()
    {
        Debug.Log("_indexFactoryBuild = " + _indexFactoryBuild);
        for(int i = 0; i < _factoryBuildingOptions.Count; i++)
        {
            if(i == _indexFactoryBuild)
            {
                _factoryBuildingOptions[i].SetActive(true);
            }
            else
            {
                _factoryBuildingOptions[i].SetActive(false);
            }
        }
    }

    IEnumerator SetFactoryOption()
    {
        switch (_factoryLvl)
        {
            case 1:
                _indexFactoryBuild = 0;
                break;
            case 5:
                _indexFactoryBuild = 1;
                break;
            case 10:
                _indexFactoryBuild = 2;
                break;
            
            default:
                break;
        }

        yield return null;
    }

    private void CalcTheCoinsEarnedOffline()
    {
        DateTime lastSessionDate = _gameSessionData.currentDateLastSession;
        TimeSpan timeOffline = DateTime.UtcNow - lastSessionDate;
        int secondsOffline = (int)timeOffline.TotalSeconds;
        _playerCoins += (secondsOffline/7) * _factoryLvl;
    }

    IEnumerator StopCar()
    {
        yield return new WaitForSeconds(2);
        _bulldozerAnim.SetBool("playanim", false);
        isMoveCar = true;
    }

    void OnApplicationPause (bool pauseStatus)
    {
        if(pauseStatus == true)
        {

        }
    }

    void OnApplicationQuit()
    {
        SetDataGame();
    }
}
