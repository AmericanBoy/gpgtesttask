﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using TMPro;

public class UIController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _coinsCount;
    [SerializeField] private TextMeshProUGUI _upgradeCost;
    [SerializeField] private TextMeshProUGUI _coinsPerDelivery;
    [SerializeField] private GameObject _panelNotEnoughtCoins;
    [SerializeField] private Button _notEnoughtCoinsBtn;

    [SerializeField] private Button _upgradeFactoryBtn;

    private IApplicationService _applicationService;

    [Inject]
    public void Init(IApplicationService applicationService)
    {
        _applicationService = applicationService;
    }

    private void Start()
    {
        _upgradeFactoryBtn.onClick.AddListener(() => 
        {
            _applicationService.UpgradeFactory();
        });

        _notEnoughtCoinsBtn.onClick.AddListener(() => 
        {
            _panelNotEnoughtCoins.SetActive(false);
        });
    }

    public void SetCoinstCount(int coins)
    {
        Debug.Log("coins = " + coins);
        _coinsCount.text =  coins.ToString();
    }

    public void SetUpgradeCost(int cost)
    {
        _upgradeCost.text = cost.ToString();
    }

    public void SetCoinsPerDepivery(int coinsPerDeliv)
    {
        _coinsPerDelivery.text = $"Gives {coinsPerDeliv} coins per delivery";
    }

    public void SetActivePanelNotEnoughtCoins()
    {
        _panelNotEnoughtCoins.SetActive(true);
    }
}
