﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Globalization;

public class GameSessionData
{
    public GameSessionData()
    {
        currentFactoryLvl = 1;
        currentCountCoins = 0;
        currentIndexFactory = 0;
    }

    public GameSessionData(int factoryLvl, int countCoins, DateTime dt, int indexFactory)
    {
        currentFactoryLvl = factoryLvl;
        currentCountCoins = countCoins;
        currentDateLastSession = dt;
        currentIndexFactory = indexFactory;
    }

    public int currentFactoryLvl { get; set; }
    public int currentCountCoins { get; set; }
    public int currentIndexFactory { get; set; }
    public DateTime currentDateLastSession { get; set; }
}
