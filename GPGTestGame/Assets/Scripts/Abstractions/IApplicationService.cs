﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IApplicationService
{
    event Action OnPlayerCoinsChanged;
    event Action OnPlayerUpgradeFactory;
    GameSessionData GameSessionData { get; }

    void UpdatePlayerCoins();
    void UpgradeFactory();

    void UpdateGameSessionData(GameSessionData newGameSessionData);

    GameSessionData GetGameSessionDataFromStorage();
}
