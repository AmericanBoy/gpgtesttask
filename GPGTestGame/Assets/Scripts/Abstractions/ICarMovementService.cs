﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICarMovementService
{
    int StartToMoveCar(GameObject car, List<Transform> pointsToMoveForCar, float speedToMoveForCar, int currentPointToMove);
}
