﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovementService : ICarMovementService
{
    private float damping = 1.0f;

    public int StartToMoveCar(GameObject car, List<Transform> pointsToMoveForCar, float speedToMoveForCar, int currentPointToMove)
    {
        var currentPoint = currentPointToMove;
        
        if(car.transform.position != pointsToMoveForCar[currentPoint].position)
        {
            float step =  speedToMoveForCar;
            car.transform.position = Vector3.MoveTowards(car.transform.position, pointsToMoveForCar[currentPoint].position, 0.6f);

            Vector3 targetDirection = pointsToMoveForCar[currentPoint].position - car.transform.position;

            float singleStep = speedToMoveForCar * Time.deltaTime;

            Vector3 newDirection = Vector3.RotateTowards(car.transform.forward, targetDirection, singleStep, 0.0f);

            car.transform.rotation = Quaternion.LookRotation(newDirection);
            
        }
        else
        {
            currentPoint = (currentPoint + 1) % pointsToMoveForCar.Count;
        }

        return currentPoint;
    }

}
