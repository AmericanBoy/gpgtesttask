﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Globalization;
public class ApplicationService : IApplicationService
{
    public event Action OnPlayerCoinsChanged;
    public event Action OnPlayerUpgradeFactory;

    public GameSessionData GameSessionData { get; set; }

    public void UpdatePlayerCoins()
    {
        OnPlayerCoinsChanged?.Invoke();
    }

    public void UpgradeFactory()
    {
        OnPlayerUpgradeFactory?.Invoke();
    }

    public void UpdateGameSessionData(GameSessionData newGameSessionData)
    {
        PlayerPrefs.SetInt("currentPlayerCoins", newGameSessionData.currentCountCoins);
        PlayerPrefs.SetInt("currentFactoryLvl", newGameSessionData.currentFactoryLvl);
        PlayerPrefs.SetInt("currentIndexFactory", newGameSessionData.currentIndexFactory);
        PlayerPrefs.SetString("currentDataLastSession", newGameSessionData.currentDateLastSession.ToString("u", CultureInfo.InvariantCulture));
        PlayerPrefs.Save();
    }

    public GameSessionData GetGameSessionDataFromStorage()
    {
        GameSessionData GameSessionData;
        if(PlayerPrefs.HasKey("currentPlayerCoins") && PlayerPrefs.HasKey("currentFactoryLvl") && PlayerPrefs.HasKey("currentDataLastSession"))
        {
            string stored = PlayerPrefs.GetString("currentDataLastSession");
            DateTime result = DateTime.ParseExact(stored, "u", CultureInfo.InvariantCulture);
            GameSessionData = new GameSessionData(PlayerPrefs.GetInt("currentPlayerCoins"), 
                                                    PlayerPrefs.GetInt("currentFactoryLvl"),
                                                        result, PlayerPrefs.GetInt("currentIndexFactory"));
        }
        else
        {
            GameSessionData = new GameSessionData();
        }

        return GameSessionData;
    }
}
