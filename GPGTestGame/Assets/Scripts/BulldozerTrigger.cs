﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class BulldozerTrigger : MonoBehaviour
{
    private IApplicationService _applicationService;

    [Inject]
    public void Init(IApplicationService applicationService)
    {
        _applicationService = applicationService;
    }

    public void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag == "Player" && gameObject.tag == "StopZone")
        {
            GameController.Instance.isMoveCar = false;
        }

        if(other.gameObject.tag == "Player" && gameObject.tag == "CoinZone")
        {
            _applicationService.UpdatePlayerCoins();
            
        }
    }
}
