using UnityEngine;
using Zenject;

public class DefaultInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<ICarMovementService>().To<CarMovementService>().AsSingle();
        Container.Bind<IApplicationService>().To<ApplicationService>().AsSingle();
    }
}